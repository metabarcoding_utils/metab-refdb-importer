#!/usr/bin/env nextflow
/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Metabarcoding references utils
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    GitLab : https://gitlab.com/metabarcoding_utils/metab-refdb-importer
----------------------------------------------------------------------------------------
*/

process DOWNLOAD_REFDB {
    label 'process_single'

    input:
    val refdb_url

    output:
    path output_file

    script:
    output_file = refdb_url - ~/^.+\//

    """
    wget ${refdb_url} -O ${output_file}
    """
}

process REFDB_FORMAT {

  label 'process_single'

  input:
  path refdb

  output:
  path "${params.refdb_alias}.fasta.gz", emit: fasta
  path "${params.refdb_alias}_taxo.tsv", emit: taxo

  script:
  """

    awk '
      BEGIN {
        FS = "${params.format_id_sep}"
        OFS = "\t"
      }

      !/>/ {
          gsub("U","T")
          print | "gzip > ${params.refdb_alias}.fasta.gz"
      }

      />/ {

          id = \$1
          print id | "gzip > ${params.refdb_alias}.fasta.gz"

          sub(/>/, "", id)

          taxo = substr(\$0, length(\$1) + 1)
          sub("^${params.format_id_sep}", "", taxo)
          gsub("${params.format_taxo_sep}", ";", taxo)

          print id, taxo > "${params.refdb_alias}_taxo.tsv"
          
      }
    ' <(zcat ${refdb})
  
  """
}

process REFDB_TRIM {

    label 'process_single'

    input:
    path refdb_sequences

    output:
    path "${params.refdb_alias}_${params.forward_primer_alias}_${params.reverse_primer_alias}.fasta.gz", emit: fasta
    path "${params.refdb_alias}_${params.forward_primer_alias}_${params.reverse_primer_alias}.log", emit: log
    
    script:
    """
      FWD_PRIMER=${params.forward_primer}
      # get reverse primer reverse complement
      REV_PRIMER=\$(echo ${params.reverse_primer} | tr "[ATGCYRSWKMBDHV]" "[TACGRYSWMKVHDB]" | rev)

      # Define primers and output files
      MIN_F=\$(( \${#FWD_PRIMER} * 2 / 3 ))
      MIN_R=\$(( \${#REV_PRIMER} * 2 / 3 ))

      LOG="${params.refdb_alias}_${params.forward_primer_alias}_${params.reverse_primer_alias}.log"
      OUTPUT="${params.refdb_alias}_${params.forward_primer_alias}_${params.reverse_primer_alias}.fasta.gz"

      ERROR="${params.trim_error}"
      MIN_LENGTH="${params.min_length}"

      zcat ${refdb_sequences} | \
          cutadapt --discard-untrimmed --max-n 0 -e "\${ERROR}" -g "\${FWD_PRIMER}" -O "\${MIN_F}" - 2> "\${LOG}" | \
          cutadapt --discard-untrimmed -e "\${ERROR}" --minimum-length \${MIN_LENGTH} -a "\${REV_PRIMER}" -O "\${MIN_R}" - 2>> "\${LOG}" | \
          gzip > "\${OUTPUT}"
    """

}

process DADA2_FORMAT {

  label 'process_single'

  input:
  path refdb_sequences
  path refdb_taxo

  output:
  path output_file

  script:
    output_file = refdb_sequences.name.replaceFirst(".fasta.gz", "_dada2.fasta.gz")

  """

    awk '
      BEGIN {FS = "\t"}
      NR == FNR {taxo_dict[\$1] = \$2; next}
      /^>/{sub(/^>/, ""); print ">"taxo_dict[\$1]";"; next}
      {print}
    ' ${refdb_taxo} <(zcat ${refdb_sequences}) | gzip > ${output_file}

  """

}

process DECIPHER_TRAIN {

    label 'process_high'

    input:
    path refdb_sequences
    path refdb_taxo

    output:
    path output_file

    script:
    output_file = refdb_sequences.name.replaceFirst(".fasta.gz", "_decipher.rds")
    
    """
    #!/usr/bin/env Rscript

    library(tidyverse)

    refs <- Biostrings::readDNAStringSet("${refdb_sequences}")

    taxo <- read_tsv("${refdb_taxo}", col_names = c("id", "taxonomy")) |> 
      mutate(taxonomy = str_replace(taxonomy, "${params.idtaxa_root_regex}", "Root;"))

    taxo <- setNames(taxo\$taxonomy, taxo\$id)

    decipher_trained <- DECIPHER::LearnTaxa(refs, taxo[names(refs)])

    # export the idtaxa trained refdb
    saveRDS(
      decipher_trained,
      file = "${output_file}"
    )

    """

}

workflow  {

  if( params.refdb_input =~ /^http|^ftp/ ) {

    DOWNLOAD_REFDB(params.refdb_input)
      .set{ refdb }

  } else {

    channel.fromPath(params.refdb_input)
      .set{ refdb }

  }
  
  REFDB_FORMAT(refdb)
    .set{ formated_refdb }

  if(!params.skip_trimming) {

    REFDB_TRIM(formated_refdb.fasta)
      .set{ trimmed_refdb }

    if(!params.skip_decipher_training) {

      DECIPHER_TRAIN(trimmed_refdb.fasta, formated_refdb.taxo)

    }

    if(!params.skip_dada2_format) {

      DADA2_FORMAT(trimmed_refdb.fasta, formated_refdb.taxo)

    }

  } else {

    if(!params.skip_decipher_training) {

      DECIPHER_TRAIN(formated_refdb.fasta, formated_refdb.taxo)

    }

    if(!params.skip_dada2_format) {

      DADA2_FORMAT(formated_refdb.fasta, formated_refdb.taxo)

    }

  }
    

}