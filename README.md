# Metabarcoding reference databases importer

## Presentation

This nextflow module download and format reference sequence databases for the taxonomic assignment worflow at https://gitlab.com/metabarcoding_utils/taxonomic-assignment. The workflow also trims the reference sequences given a primer pair and generate a DECIPHER trained or a dada2 format version of the trimmed reference database to later assign with IDTAXA or dada2 respectively.

## Quick start

### Prerequisites

You need nextflow >= 23.04.1 installed on your system and cutadapt >= 3.1. Other tools such as gawk and wget need to be installed but that is already the case for the majority of Linux distributions. If you are working on ABiMS, all you need is already installed. See below for ABiMS cluster instructions.

### Generic usage

``` bash
nextflow \
  run https://gitlab.com/metabarcoding_utils/metab-refdb-importer \
  -r main \
  -params-file parameters.yaml
```

If you change a parameter and want to rerun the workflow, use the option `-resume` to avoid having to rerun everything.

The worflow parameters are defined in `parameters.yaml` or in any other `yaml` file specified after `-params-file`. You will find [here](param_templates/) example of parameter files to format and extract the 18S V4 (TAReuk454FWD1 and TAReukREV3 primers) from PR2 and EukRibo. More details in the section parameters.

## On ABiMS cluster

To be able to run this workflow on ABiMS cluster you need first to make nextflow availalbe with the following command:

```bash
module load nextflow/23.04.1
```

After that you can simply run the latest version of the workflow with the command below. `abims.config` is available on this repository in conf_templates directory. Remember to lauch this command either with `srun` or in a bash script launched with `sbatch`.

``` bash
nextflow \
  run https://gitlab.com/metabarcoding_utils/metab-refdb-importer \
  -r main \
  -c abims.config \
  -params-file parameters.yaml
```

## Parameters

### Reference database info

* `refdb_input`: reference database URL or path (local)
* `refdb_alias`: name to refer to the reference database. This name will be used in the output file.

### export folder
* `outdir`: path to the directory for the outputs

### steps to skip

* `skip_trimming`: set to `true` to skip sequence trimming step (default: true)
* `skip_decipher_training`: set to `true` to skip DECIPHER training step (default: true)
* `skip_dada2_format`: set to `true` to skip dada2 format step (default: true)

### input format

* `format_id_sep`: regular expression indicating the separator between reference sequence id and taxonomy in header
* `format_taxo_sep`: regular expression indicating the taxonomic rank separator

### Primers for trimming

* `forward_primer`:  5'-3' oriented forward primer sequence
* `reverse_primer`: 5'-3' oriented reverse primer sequence
* `forward_primer_alias`: forward primer name. It will be used to name trimming output file
* `reverse_primer_alias`: reverse primer name. It will be used to name trimming output file

### cutadapt parameters (trimming)

* `trim_error`: error rate for primer matching (value between 0 and 1)
* `min_length`: minimum length for a sequence to be kept after trimming